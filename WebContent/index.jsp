<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Health Insurance Quote</title>
</head>
<body>
    <h2>Welcome to <strong>HIPQG</strong>, Health Insurance Premium Quote Generator
    </h2>

   <form action="HealthInsuranceCalculatorServlet" method="post">
    <table border="0">
        
        <tbody>
          
            <tr>
                <td>
                        <strong>Name:</strong>
                </td>
           		<td>
                        <input type="text" name="name"/>
                </td>
            </tr>
            <tr>
                <td>
                        <strong>Gender:</strong>
                        </td>
                        		<td>
                        <input type="radio" name="gender" value="male" checked> Male
						  <input type="radio" name="gender" value="female"> Female
						  <input type="radio" name="gender" value="other"> Other
                </td>
            </tr>
            <tr>
                <td>
                        <strong>Age:</strong>
                        </td>
                        		<td>
                        <input type="text" name="age"/>
                </td>
            </tr>
            <tr>
                <td>
                        <strong>Current health:</strong>
                        </td>
                        		<td>
                        <table>
                        	<tr>
                        		<td>
                        			Hyper tension:
                        		</td>
                        		<td>
                        			<input type="radio" name="tension" value="true" > Yes
						            <input type="radio" name="tension" value="false" checked> No
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			Blood Pressure:
                        			</td>
                        		<td>
                        			<input type="radio" name="pressure" value="true" > Yes
						            <input type="radio" name="pressure" value="false" checked> No
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			Blood Sugar:
                        			</td>
                        		<td>
                        			<input type="radio" name="sugar" value="true" > Yes
						            <input type="radio" name="sugar" value="false" checked> No
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			Overweight:
                        			</td>
                        		<td>
                        			<input type="radio" name="weight" value="true" > Yes
						            <input type="radio" name="weight" value="false" checked> No
                        		</td>
                        	</tr>
                        </table>
                </td>
            </tr>
            <tr>
                <td>
                        <strong>Habits:</strong>
                        </td>
                        		<td>
                        <table>
                        	<tr>
                        		<td>
                        			Smoking:
                        			</td>
                        		<td>
                        			<input type="radio" name="smoking" value="true" > Yes
						            <input type="radio" name="smoking" value="false" checked> No
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			Alcohol:
                        			</td>
                        		<td>
                        			<input type="radio" name="alcohol" value="true" > Yes
						            <input type="radio" name="alcohol" value="false" checked> No
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			Daily Exercise:
                        			</td>
                        		<td>
                        			<input type="radio" name="exercise" value="true" > Yes
						            <input type="radio" name="exercise" value="false" checked> No
                        		</td>
                        	</tr>
                        	<tr>
                        		<td>
                        			Drugs:
                        			</td>
                        		<td>
                        			<input type="radio" name="drugs" value="true" > Yes
						            <input type="radio" name="drugs" value="false" checked> No
                        		</td>
                        	</tr>
                        </table>
                </td>
            </tr>
            <tr>
            	<td>
            		<input type="submit" value="Submit"/>
            	</td>
            </tr>
        </tbody>
    </table>
   </form>

</body>
</html>