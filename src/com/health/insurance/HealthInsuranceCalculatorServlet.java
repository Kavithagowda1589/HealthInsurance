package com.health.insurance;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HealthInsuranceCalculator
 */

public class HealthInsuranceCalculatorServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final int ZERO = 0;
	private static final int ONE = 1;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int FIVE = 5;
	private static final int FIVE_THOUSAND = 5000;
	private static final int FOURTYFIVE = 45;
	private static final int EIGHTEEN = 18;
	private static final int HUNDRED = 100;
	private static final int FOURTY = 40;
	private static final int TWENTY = 20;
	private static final int TEN = 10;
	private static final String MALE = "male";
	private static final String TEXT_FORMAT = "text/html";
	private static final String NAME = "name";
	private static final String GENDER = "gender";
	private static final String AGE = "age";
	private static final String TENSION = "tension";
	private static final String PRESSURE = "pressure";
	private static final String SUGAR = "sugar";
	private static final String WEIGHT = "weight";
	private static final String SMOKING = "smoking";
	private static final String ALCOHOL = "alcohol";
	private static final String EXERCISE = "exercise";
	private static final String DRUGS = "drugs";
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType(TEXT_FORMAT);  
	    PrintWriter out = response.getWriter();  
	    long premium = ZERO;
	    int basePremium = FIVE_THOUSAND;
	    String name = request.getParameter(NAME);  
	    String gender = request.getParameter(GENDER);  
	    int age = Integer.parseInt(request.getParameter(AGE));
	    boolean tension = Boolean.parseBoolean(request.getParameter(TENSION));
	    boolean pressure = Boolean.parseBoolean(request.getParameter(PRESSURE));
	    boolean sugar = Boolean.parseBoolean(request.getParameter(SUGAR));
	    boolean weight = Boolean.parseBoolean(request.getParameter(WEIGHT));
	    boolean smoking = Boolean.parseBoolean(request.getParameter(SMOKING));
	    boolean alcohol = Boolean.parseBoolean(request.getParameter(ALCOHOL));
	    boolean exercise = Boolean.parseBoolean(request.getParameter(EXERCISE));
	    boolean drugs = Boolean.parseBoolean(request.getParameter(DRUGS));
	    int percentIncrease = ZERO;
	    int premiumCalculator = ZERO;
		if(age < EIGHTEEN) {
			premium = basePremium;
		} else {
			percentIncrease = percentageOnAge(age);
			percentIncrease = percentIncrease + percentageOnGender(gender);
			percentIncrease = percentIncrease + percentageOnHealth(tension,pressure,sugar,weight);
			percentIncrease = percentIncrease + percentageOnHabits(alcohol,smoking,exercise,drugs);
			premiumCalculator = (basePremium/HUNDRED) * percentIncrease;
			premium = basePremium + premiumCalculator;
		}
		out.println("Health Insurance Premium for "+name+": Rs." +premium);
	}
	private int percentageOnAge(int age) {
		if( age >= EIGHTEEN && age < FOURTY) {
			return TEN;
		} else {
			int increaseOnAge = ZERO;
			int i = FOURTYFIVE;
			while(i<age) {
				increaseOnAge += ONE;
				i += FIVE;
			}
			return TWENTY * increaseOnAge;
		}
		
	}
	private int percentageOnGender(String gender) {
		if((gender.toLowerCase()).equals(MALE)) {
			return TWO;
		}
		return ZERO;
	}

	private int percentageOnHealth(boolean tension, boolean pressure, boolean sugar, boolean weight) {
		int percentIncrease = ZERO;
		if(tension) {
			percentIncrease = ONE;
		}
		if(pressure) {
			percentIncrease = percentIncrease + ONE;
		}
		if(sugar) {
			percentIncrease = percentIncrease + ONE;
		}
		if(weight) {
			percentIncrease = percentIncrease + ONE;
		}
		return percentIncrease;
	}
	private int percentageOnHabits(boolean alcohol, boolean smoking, boolean exercise, boolean drugs) {
		int percentIncrease = ZERO;
		if(exercise) {
			percentIncrease = percentIncrease - THREE;
		}
		if(alcohol) {
			percentIncrease = percentIncrease + THREE;
		}
		if(smoking) {
			percentIncrease = percentIncrease + THREE;
		}
		if(drugs) {
			percentIncrease = percentIncrease + THREE;
		}
		return percentIncrease;
	}
}
